Este repositorio contiene una aplicación Django para gestionar la web de la [Estelcon](https://estelcon2019.sociedadtolkien.org), la convención anual de la [Sociedad Tolkien Española](https://www.sociedadtolkien.org).

Para cualquier información o aclaración, contactar con el [grupo de trabajo Web](https://www.sociedadtolkien.org/comisiones/).